# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MusicApp::Application.config.secret_key_base = 'e8ccc06ef1c0f90e992925c75c994b488f6a95c50761af1b56208ba2fbf2bbc6d48113aa2beecf1a8e403a50610b365e606c5e300bde321ce433ad4d6a614790'
