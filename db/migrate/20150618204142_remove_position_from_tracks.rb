class RemovePositionFromTracks < ActiveRecord::Migration
  def change
    remove_column :tracks, :position
  end
end
