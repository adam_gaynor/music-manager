class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :album_title, null: false
      t.integer :band_id, null: false
      t.string :album_type, null: false

      t.timestamps
    end

    add_index :albums, :album_title
    add_index :albums, :band_id
  end
end
