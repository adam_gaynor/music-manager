# == Schema Information
#
# Table name: albums
#
#  id          :integer          not null, primary key
#  album_title :string(255)      not null
#  band_id     :integer          not null
#  album_type  :string(255)      not null
#  created_at  :datetime
#  updated_at  :datetime
#

class Album < ActiveRecord::Base

  validates :album_title, :band_id, :album_type, presence: true

  belongs_to(
    :band,
    :class_name => "Band",
    :foreign_key => :band_id,
    :primary_key => :id
  )

  has_many(
    :tracks,
    :dependent => :destroy,
    :class_name => "Track",
    :foreign_key => :album_id,
    :primary_key => :id
  )

end
