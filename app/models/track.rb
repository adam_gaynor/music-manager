# == Schema Information
#
# Table name: tracks
#
#  id          :integer          not null, primary key
#  track_title :string(255)      not null
#  album_id    :integer          not null
#  track_type  :string(255)      not null
#  lyrics      :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Track < ActiveRecord::Base

  validates :track_title, :album_id, :track_type, presence: true

  belongs_to(
    :album,
    :class_name => "Album",
    :foreign_key => :album_id,
    :primary_key => :id
  )

  has_many(
    :notes,
    :dependent => :destroy,
    :class_name => "Note",
    :foreign_key => :track_id,
    :primary_key => :id
  )
end
