# == Schema Information
#
# Table name: notes
#
#  id           :integer          not null, primary key
#  track_id     :integer          not null
#  user_id      :integer          not null
#  note_content :text             not null
#

class Note < ActiveRecord::Base

  validates :track_id, :user_id, :note_content, presence: true

  belongs_to(
    :author,
    :class_name => "User",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  belongs_to(
    :track,
    :class_name => "Track",
    :foreign_key => :track_id,
    :primary_key => :id
  )

  
end
