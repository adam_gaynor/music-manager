# == Schema Information
#
# Table name: bands
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Band < ActiveRecord::Base

  validates :name, presence: { message: "Band name can't be blank" }


  has_many(
    :albums,
    :dependent => :destroy,
    :class_name => "Album",
    :foreign_key => :band_id,
    :primary_key => :id

  )
end
