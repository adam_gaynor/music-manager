class NotesController < ApplicationController

  before_action :require_current_user!

  def create
    parameters = {}
    notes_params.each do |key,value|
      parameters[key] = value
    end
    parameters[:user_id] = current_user.id
    @note = Note.new(parameters)
    @track = Track.find(@note.track_id)

    if @note.save
      redirect_to track_url(@track)
    else
      redirect_to track_url(@track)
    end
  end

  # def new
  #   @track = Track.new
  #   @album = Album.find(params[:album_id])
  #   @track.album_id = @album.id
  #   render :new
  # end

  # def edit
  #   @track = Track.find(params[:id])
  #   @album = Album.find(@track.album_id)
  #   render :edit
  # end

  def show
    @track = Track.find(params[:id])
    render :show
  end

  # def update
  #   @track = Track.find(params[:id])
  #   if @track.update(track_params)
  #     redirect_to track_url(@track)
  #   else
  #     @album = Album.find(@track.album_id)
  #     render :edit
  #   end
  # end

  def destroy
    @note = Note.find(params[:id])
    unless @note.author == current_user
      return
    end
    @track = Track.find(@note.track_id)
    @note.destroy
    redirect_to track_url(@track)
  end


  private

  def notes_params
    params.require(:note).permit(:track_id, :note_content)
  end
end
