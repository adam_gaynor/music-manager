class TracksController < ApplicationController

  before_action :require_current_user!

  def create
    @track = Track.new(track_params)
    if @track.save
      redirect_to track_url(@track)
    else
      @album = Album.find(@track.album_id)
      render :new
    end
  end

  def new
    @track = Track.new
    @album = Album.find(params[:album_id])
    @track.album_id = @album.id
    render :new
  end

  def edit
    @track = Track.find(params[:id])
    @album = Album.find(@track.album_id)
    render :edit
  end

  def show
    @track = Track.find(params[:id])
    render :show
  end

  def update
    @track = Track.find(params[:id])
    if @track.update(track_params)
      redirect_to track_url(@track)
    else
      @album = Album.find(@track.album_id)
      render :edit
    end
  end

  def destroy
    @track = Track.find(params[:id])
    @album = Album.find(@track.album_id)
    @track.destroy
    redirect_to album_url(@album.id)
  end


  private

  def track_params
    params.require(:track).permit(:track_title, :album_id, :track_type, :lyrics)
  end
end
