class AlbumsController < ApplicationController

  before_action :require_current_user!

  def create
    @album = Album.new(album_params)
    if @album.save
      redirect_to album_url(@album)
    else
      @band = Band.find(@album.band_id)
      render :new
    end
  end

  def new
    @album = Album.new
    @band = Band.find(params[:band_id])
    @album.band_id = @band.id
    render :new
  end

  def edit
    @album = Album.find(params[:id])
    @band = Band.find(@album.band_id)
    render :edit
  end

  def show
    @album = Album.find(params[:id])
    render :show
  end

  def update
    @album = Album.find(params[:id])
    if @album.update(album_params)
      redirect_to album_url(@album)
    else
      @band = Band.find(@album.band_id)
      render :edit
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @band = Band.find(@album.band_id)
    @album.destroy
    redirect_to band_url(@band.id)
  end


  private

  def album_params
    params.require(:album).permit(:album_title, :band_id, :album_type)
  end
end
